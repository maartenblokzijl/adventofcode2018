package com.blokzijl.advent;

import java.io.IOException;
import java.math.BigInteger;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntConsumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.blokzijl.advent.util.InputReader.inputAsStrings;
import static java.util.stream.Collectors.counting;

public class DayThree {

    private static BigInteger[][] fabric = new BigInteger[1000][1000];
    private static List<List<BigInteger>> input;

    public static void main(String... args) {
        try {
            readInput();
            doPartOne();
            doPartTwo();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private static void readInput() throws IOException {
        Instant start = Instant.now();

        input = inputAsStrings("daythree.txt")
                .stream()
                .map(parseInputLine())
                .collect(Collectors.toList());

        Instant end = Instant.now();
        System.out.println("Duration input reading: " + Duration.between(start, end).toMillis());
    }

    private static void doPartOne() {
        Instant start = Instant.now();

        input.forEach(addToFabric());

        long resultPartOne = Arrays.stream(fabric).flatMap(Arrays::stream).filter(BigInteger.ZERO::equals).count();

        Instant end = Instant.now();
        System.out.println("Duration part one: " + Duration.between(start, end).toMillis());
        System.out.println("Found answer to part one: " + resultPartOne);
    }

    private static void doPartTwo() {
        Instant start = Instant.now();

        Map<BigInteger, Long> mapFabric = Arrays.stream(fabric).flatMap(Arrays::stream).collect(
                Collectors.groupingBy(
                        id -> id == null ? BigInteger.ZERO : id,
                        counting()
                )
        );
        BigInteger resultPartTwo = input.stream().filter(checkSizeOnFabric(mapFabric)).findAny().map(i -> i.get(0)).orElseThrow();
        Instant end = Instant.now();
        System.out.println("Duration part two: " + Duration.between(start, end).toMillis());
        System.out.println("Found answer to part two: " + resultPartTwo);
    }

    // zeroth element is id
    // first is column
    // second is row
    // third is width
    // fourth is height
    private static Function<String, List<BigInteger>> parseInputLine() {
        return line -> Arrays.stream(
                line.replaceFirst("#", "").split("[@,:x]"))
                .map(String::trim)
                .map(Integer::valueOf)
                .map(BigInteger::valueOf)
                .collect(Collectors.toList());
    }

    private static Consumer<List<BigInteger>> addToFabric() {
        return inputLine -> IntStream.range(inputLine.get(1).intValue(), inputLine.get(1).add(inputLine.get(3)).intValue())
                .forEach(x -> IntStream.range(inputLine.get(2).intValue(), inputLine.get(2).add(inputLine.get(4)).intValue())
                        .forEach(fillFabricField(inputLine.get(0), x)));
    }

    private static IntConsumer fillFabricField(BigInteger id, Integer x) {
        return y -> {
            if (fabric[x][y] == null) {
                fabric[x][y] = id;
            } else {
                fabric[x][y] = BigInteger.ZERO;
            }
        };
    }

    private static Predicate<List<BigInteger>> checkSizeOnFabric(Map<BigInteger, Long> mapFabric) {
        return inputLine -> inputLine.get(3).multiply(inputLine.get(4))
                .equals(BigInteger.valueOf(Optional.ofNullable(mapFabric.get(inputLine.get(0))).orElse(0L)));
    }
}
