package com.blokzijl.advent.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

public class InputReader {

    private static File getFile(String filename) {
        ClassLoader loader = InputReader.class.getClassLoader();
        return new File(Objects.requireNonNull(loader.getResource(filename)).getFile());
    }

    public static List<String> inputAsStrings(String filename) throws IOException {
        Scanner scanner = new Scanner(getFile(filename));
        List<String> result = new ArrayList<>();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            result.add(line);
        }
        scanner.close();

        return result;
    }

    public static List<Integer> inputAsIntegers(String filename) throws IOException {
        return inputAsStrings(filename).stream().mapToInt(Integer::valueOf).boxed().collect(Collectors.toList());
    }
}
