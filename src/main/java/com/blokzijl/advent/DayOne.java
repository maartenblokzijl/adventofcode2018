package com.blokzijl.advent;

import java.util.*;
import java.util.stream.Stream;

import static com.blokzijl.advent.util.InputReader.inputAsIntegers;

public class DayOne {

    private static List<Integer> sumList = new ArrayList<>(Collections.singletonList(0));
    private static List<Integer> input;

    public static void main(String... args) {
        try {
            input = inputAsIntegers("dayone.txt");
            Integer part1 = input.stream().reduce((a, b) -> a + b).orElseThrow();
            System.out.println("Result  part 1 = " + part1);

            Stream.generate(() -> input).flatMap(Collection::stream)
                    .reduce(DayOne::doStuff).orElseThrow();

        } catch (Throwable t) {
            System.out.println(t.getMessage());
        }
    }

    // This is ugly, but it works
    private static Integer doStuff(Integer a, Integer b) {
        if (sumList.contains(a)) {
            System.out.println("Found: " + a);
            throw new RuntimeException("Yay!");
        } else {
            sumList.add(a);
        }
        return a + b;
    }
}
