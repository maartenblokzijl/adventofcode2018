package com.blokzijl.advent;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.blokzijl.advent.util.InputReader.inputAsStrings;
import static java.util.stream.Collectors.*;

public class DayTwo {

    private static List<String> input;
    private static Integer duplicateCount = 0;
    private static Integer triplicateCount = 0;

    private static List<String> expand(String s) {
        return s.codePoints()
                .mapToObj(codePoint -> Character.toString((char) codePoint))
                .collect(toList());
    }

    public static void main(String... args) {
        try {
            input = inputAsStrings("daytwo.txt");
            doPartOne();
            doPartTwo();
        } catch (Throwable t) {
            System.out.println(t.getMessage());
        }
    }

    private static void doPartTwo() {
        String result = input.stream()
                .filter(DayTwo::doStuff)
                .peek(System.out::println)
                .reduce((line1, line2) -> {
                    StringBuilder output = new StringBuilder();
                    Iterator<String> iter1 = expand(line1).iterator();
                    Iterator<String> iter2 = expand(line2).iterator();
                    while (iter1.hasNext() && iter2.hasNext()) {
                        String letter1 = iter1.next();
                        String letter2 = iter2.next();
                        if (letter1.equals(letter2)) {
                            output.append(letter1);
                        }
                    }
                    return output.toString();
                }).orElseThrow();
        System.out.println(result);
    }

    private static boolean doStuff(String searchLine) {
        List<Integer> results = new ArrayList<>();
        input.forEach(line -> {
            Iterator<String> searchIterator = expand(searchLine).iterator();
            Iterator<String> lineIterator = expand(line).iterator();
            Integer count = 0;
            while (searchIterator.hasNext() && lineIterator.hasNext()) {
                if (searchIterator.next().equals(lineIterator.next())) {
                    count++;
                }
            }
            results.add(count);
        });
        return results.contains(25);
    }

    private static void doPartOne() {
        input.forEach(
                line -> {
                    Map<String, Long> counts = expand(line).stream().collect(
                            groupingBy(
                                    letter -> letter,
                                    counting()
                            )
                    );
                    if (counts.containsValue(2L)) {
                        duplicateCount++;
                    }
                    if (counts.containsValue(3L)) {
                        triplicateCount++;
                    }
                });
        System.out.println("Duplicates: " + duplicateCount);
        System.out.println("Triplicates: " + triplicateCount);
        System.out.println("\"Checksum\": " + duplicateCount * triplicateCount);
    }
}
